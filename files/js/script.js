          if(window.analytics=window.analytics||[],window.analytics.included)window.console&&console.error&&console.error("analytics.js included twice");else{window.analytics.included=!0,window.analytics.methods=["identify","group","track","page","pageview","alias","ready","on","once","off","trackLink","trackForm","trackClick","trackSubmit"],window.analytics.factory=function(a){return function(){var n=Array.prototype.slice.call(arguments);return n.unshift(a),window.analytics.push(n),window.analytics}};for(var i=0;i<window.analytics.methods.length;i++){var key=window.analytics.methods[i];window.analytics[key]=window.analytics.factory(key)}window.analytics.load=function(a){var n=document.createElement("script");n.type="text/javascript",n.async=!0,n.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+a+"/analytics.min.js";var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(n,t)},window.analytics.SNIPPET_VERSION="2.0.9",
          window.analytics.load("a0AzVNqhmf")}
       
          "use strict";

          !function() {
            var t = window.driftt = window.drift = window.driftt || [];
            if (!t.init) {
              if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
              t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
              t.factory = function(e) {
                return function() {
                  var n = Array.prototype.slice.call(arguments);
                  return n.unshift(e), t.push(n), t;
                };
              }, t.methods.forEach(function(e) {
                t[e] = t.factory(e);
              }), t.load = function(t) {
                var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
                o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
                var i = document.getElementsByTagName("script")[0];
                i.parentNode.insertBefore(o, i);
              };
            }
          }();

          // Only load drift if we are not on the signup page.
          if (window.location && window.location.pathname !== "/accept-invitation") {
            drift.SNIPPET_VERSION = '0.3.1';
            drift.load('vyddvzm4gyn2');
          }
       
          (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          '../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-KXF5F7W');
       
          const isProduction = window.location.href.includes('frontapp.com');

          window.addEventListener('load', () => {
            if (!window.cb_data)
              return;

            const EU = ['at', 'be', 'bg', 'cy', 'cz', 'dk', 'de', 'ee', 'es', 'fi', 'fr', 'gb', 'gr', 'hu', 'hr', 'ie', 'it', 'lv', 'lt', 'lu', 'mt', 'nl', 'po', 'pt', 'ro', 'se', 'si', 'sk'];
            const countryCode = window.cb_data.geoIP && window.cb_data.geoIP.countryCode && window.cb_data.geoIP.countryCode.toLowerCase();

            if (!EU.includes(countryCode))
              return;

            window.cookieconsent.initialise({
              'position': 'bottom-left',
              'content': {
                'allow': 'Allow cookies',
                'deny': 'Decline',
                'close': '&#x274c;',
                'link': 'Learn more',
                'href': '#'
              },
              'type': 'info'
            });
          });

          if (window.drift && window.drift.on) {
            window.drift.on('scheduling:meetingBooked', function(data) {
              const userEmail = window.localStorage.getItem('drift-email');
              Front.hubspot.trackDemo({email: userEmail, conversationId: data.conversationId});
              Front.sendEvent('demo-request');
            });
      
            window.drift.on('emailCapture', function (e) {
              const userEmail = e.data.email;
              Front.hubspot._hsq().push(['identify', { email: userEmail }]);
              window.localStorage.setItem('drift-email', userEmail);
            });
          }
        

          const Front = {
            getQueryVariable(variable) {
              var query = window.location.search.substring(1),
                vars = query.split('&');
          
              for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split('=');
          
                if (decodeURIComponent(pair[0]) === variable)
                  return decodeURIComponent(pair[1]);
              }
          
              return null;
            },
            generateGuid() {
              return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8); // eslint-disable-line

                return v.toString(16);
              });
            },
            getFaid() {
              return window.localStorage ? window.localStorage.getItem('front.aid') : null;
            },
            setFaid() {
              let uid = this.getFaid();
              if (window.localStorage && !uid)
                try {
                  uid = Front.generateGuid();
                  window.localStorage.setItem('front.aid', uid);
                  if (window.axios && window.cb_data) {
                    window.axios.defaults.headers.common['front-anonymous-id'] = uid;
                    Front.sendEvent('clearbit-reveal', window.cb_data);
                  }
                } catch (e) {
                  //
                }
              
              if (uid && window.axios)
                window.axios.defaults.headers.common['front-anonymous-id'] = uid;
            },
            setCookie(name, value, days) {
              const date = new Date();
              date.setTime(date.getTime() + (days*24*60*60*1000));
              const expires = '; expires=' + date.toGMTString();
              document.cookie = name + '=' + value + ' ' + expires + ';path=/';
            },
            readCookie(name) {
              var n = name + '=';
              var cookie = document.cookie.split(';');
              for(var i=0;i < cookie.length;i++) {
                var c = cookie[i];
                while (c.charAt(0)==' '){c = c.substring(1,c.length);}
                if (c.indexOf(n) == 0){return c.substring(n.length,c.length);}
              }
          
              return null;
            },
            gclid: {
              current: function() {
                return Front.getQueryVariable('gclid');
              },
              record: function() {
                const currentGclid = this.current();
                if (currentGclid) {
                  const gclsrc = Front.getQueryVariable('gclsrc');
                  if (!gclsrc || gclsrc.indexOf('aw') !== -1)
                    Front.setCookie('gclid', currentGclid, 90);
                }
              },
              get: function() {
                return Front.readCookie('gclid');
              },
            },
            hubspot: {
              getHubspotUtk: function() {
                const cookie = Front.readCookie('hubspotutk');
                return cookie || null;
              },
              formApiUrl: '#',
              _hsq: function () {
                let _hsq = window._hsq = window._hsq || [];
                return _hsq;
              },
              convertModel: function(model) {
                const hubspotFields = [];
          
                for (const fieldName in model)
                  hubspotFields.push({name: fieldName, value: model[fieldName]});
          
                return hubspotFields;
              },
              trackDemo: function(data) {
                this._hsq().push(['identify', {
                  email: data.email
                }]);
            
                this._hsq().push(['trackEvent', {
                  id: 'demo_requested',
                  value: data.conversationId
                }]);
              },
              postForm: function(tracking) {
                // We don't want ghost inspector contacts syncing into Hubspot.
                const ghostInspectorPattern = /^fronttestcompany-ghostinspector[a-zA-Z0-9]*@email.ghostinspector.com$/i;
                if (ghostInspectorPattern.test(tracking.email))
                  return;
          
                const hubspotModel = {
                  email: tracking.email,
                  do_not_contact: tracking.do_not_contact || false,
                  hull_gclid: tracking.gclid || Front.gclid.get() || '',
                  affiliate: tracking.affiliate || Front.getQueryVariable('affiliate') || '',
                  page_url_form_: window.location.pathname,
                  front_phone_number: tracking.front_phone_number || '',
                  utm_campaign: Front.getQueryVariable('utm_campaign') || '',
                  utm_source: Front.getQueryVariable('utm_source') || '',
                  utm_medium: Front.getQueryVariable('utm_medium') || '',
                  utm_term: Front.getQueryVariable('utm_term') || ''
                };
          
                const hubspotFields = this.convertModel(hubspotModel);
          
                /**
                 * Use Hubspot Tracking Code API to submit UTM data
                 * Will associate page views with hubspot cookie (hubspotutk)
                 * function call ('identify') stores the data in the tracker, but the data is not actually passed to HubSpot with this call.
                 */
                this._hsq().push(['identify', {
                  email: tracking.email
                }]);
          
                /**
                 * The data will only be passed when tracking a pageview or an event
                 * (with either the trackPageView or trackEvent functions).
                 */
                this._hsq().push(['trackEvent', {
                  id: 'signup_form_completed'
                }]);
          
                return window.axios({
                  url: this.formApiUrl,
                  method: 'post',
                  timeout: 2000,
                  data: {
                    fields: hubspotFields,
                    context: {
                      pageUri: window.location.href,
                      hutk: this.getHubspotUtk(),
                    }
                  }
                }).catch(() => {}); // Silently catch the error to avoid breaking sign up flow
              }
            },
            sendEvent: function (name, value) {
              const faid = this.getFaid();
              
              if (faid && window.axios && isProduction)
                return window.axios({
                  url: '#',
                  method: 'post',
                  data: {
                    name: name,
                    value: JSON.stringify(value),
                    time: Date.now()
                  }
                }).catch(() => {});

              // So the return signature is consistent.
              return Promise.resolve();
            },
            trackSignup: function (tracking) {

              const payload = Object.assign({}, tracking, {gclid: Front.gclid.get()})

              if (window.analytics)
                window.analytics.track('Sign-up', tracking);

              if (window.dataLayer) {
                window.dataLayer.push(Object.assign({}, tracking, {event: 'signup'}));
              }

              // Capterra conversion tracking
              let capterra_vkey = '1742e49cb108a2e4712489bfde348311',
                capterra_vid = '2093635',
                capterra_prefix = (('#');

              (function() {
                let ct = document.createElement('script'); ct.type = 'text/javascript'; ct.async = true;
                ct.src = capterra_prefix + '/capterra_tracker.js?vid=' + capterra_vid + '&vkey=' + capterra_vkey;
                let s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ct, s);
              })();
            },
            trackDemoRequest: function (data) {
              if (window.dataLayer) {
                window.dataLayer.push(Object.assign({}, data, {event: 'demo_request'}));
              }
            }
          };

          Front.setFaid();

          // Track GCLID
          if (Front.gclid.current()) {
            Front.gclid.record();
            Front.sendEvent('gclid', Front.gclid.get());
          }

          // Segment Analytics.
          if (window.analytics) {
            let properties = {};
            let search = window.location.search;
            if (search.indexOf('referrer=') >= 0) {
              properties.referrer = search.split('referrer=')[1].split('&')[0];

              // FIXME: We need this fix until Segment updates their code for referrer tracking in GA
              // cf. https://github.com/segment-integrations/analytics.js-integration-google-analytics/pull/23
              if (window.ga) {
                window.ga('set', 'referrer', properties.referrer);
              }
            }

            if (window.ga) {
              window.ga((tracker) => {
                var client_id = tracker.get('clientId');
                window.analytics.identify({client_id: client_id});
                window.analytics.page(properties);
              });
            } else {
              window.analytics.page(properties);
            }
          }



/*------------

$(document).ready(function() {
  $(window).scroll(function() {
    if ($(document).scrollTop() > 50) {
      $(".header-section").addClass("active");
    } else {
      $(".header-section").removeClass("active");
    }
  });
});
--------*/
/*--------------------*/


